import axios from 'axios';
import { getHeaders, logout, HEADERS_KEYS as headersKeys } from './auth';

const api = axios.create({
  baseURL: 'https://empresas.ioasys.com.br/api/v1/'
});

api.interceptors.request.use(async config => {
  const headers = getHeaders();

  if (headers) {
    headersKeys.forEach(key => (config.headers[key] = headers[key]));
  }

  return config;
});

api.interceptors.response.use(
  response => response,
  error => {
    if (error.response.status !== 401) {
      return Promise.reject(error);
    }

    // 401
    logout();
    window.location = '/login';
  }
);

export const getEnterprises = async ({ type, name }) => {
  let params = {};

  if (type) {
    params.enterprise_types = type;
  }

  if (name) {
    params.name = name;
  }

  return await api.get('enterprises', { params });
};

export const getEnterpriseById = async enterpriseId =>
  await api.get(`enterprises/${enterpriseId}`);

export default api;
