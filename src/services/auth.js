// access-token, client, uid
export const HEADERS_KEYS = ['access-token', 'client', 'uid'];

export const isAuthenticated = () =>
  localStorage.getItem('access-token') !== null;

// get all headers in a single object
export const getHeaders = () =>
  HEADERS_KEYS.reduce((headers, key) => {
    headers[key] = localStorage.getItem(key);
    return headers;
  }, {});

// set all headers
export const login = headers =>
  HEADERS_KEYS.forEach(key => localStorage.setItem(key, headers[key]));

// set all headers
export const logout = () =>
  HEADERS_KEYS.forEach(key => localStorage.removeItem(key));
