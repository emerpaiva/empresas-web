import styled from 'styled-components';
import media from 'styled-media-query';
import { Link } from 'react-router-dom';
import { ReactComponent as SearchIcon } from '../../assets/images/svg/ic-search.svg';

export const Header = styled.header`
  background-image: linear-gradient(to bottom, #ee4c77, var(--night-blue) 300%);
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 2rem;
  height: 9rem;

  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
`;

export const FlexSpaceHacker = styled.div`
  flex: 0 1 20rem;

  ${media.lessThan('medium')`
    display: none;
  `}
`;

export const LinkLogo = styled(Link)`
  width: 20rem;

  & > img {
    width: 100%;
  }
`;

export const Search = styled(SearchIcon)`
  cursor: pointer;
`;
