import React from 'react';

import InputForm from '../InputForm';

import LogoNav from '../../assets/images/png/logo-nav.png';

import * as S from './styled';

function Header({ handlerSearch }) {
  return (
    <S.Header>
      <S.FlexSpaceHacker />
      <S.LinkLogo to='/dashboard'>
        <img src={LogoNav} alt='ioasys' />
      </S.LinkLogo>
      <InputForm
        type='search'
        placeholder='Pesquisar'
        onKeyDown={handlerSearch}
        icon={S.Search}
        large={true}
      />
    </S.Header>
  );
}

export default Header;
