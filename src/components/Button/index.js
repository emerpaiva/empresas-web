import React from 'react';

import * as S from './styled';

function Button({
  type,
  placeholder,
  icon,
  children,
  disabled,
  ...otherProps
}) {
  return (
    <S.Button type={type} disabled={disabled} {...otherProps}>
      {children}
    </S.Button>
  );
}

export default Button;
