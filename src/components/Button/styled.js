import styled, { css } from 'styled-components';

export const Button = styled.button`
  appearance: none;
  background-color: #57bbbc;
  border-radius: 0.3rem;
  box-shadow: 0 0.5rem 0.5rem rgba(0, 0, 0, 0.2);
  border: none;
  color: #fff;
  cursor: pointer;
  font-size: 1.8rem;
  font-weight: bold;
  text-transform: uppercase;
  transition: all 0.1s linear;
  padding: 1.4rem;
  min-width: 25rem;

  &:hover {
    transform: translateY(1px);
  }

  ${props =>
    props.disabled &&
    css`
      background-color: #75c7c7;
      cursor: not-allowed;
    `}
`;
