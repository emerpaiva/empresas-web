import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';

import { getEnterpriseById } from '../../services/api';

import { getFallbackName } from '../../utils/fallback';

import Preload from '../Preload';

import * as S from './styled';

function Enterprise() {
  const [enterprise, setEnterprise] = useState(null);
  const { enterpriseId } = useParams();

  useEffect(() => {
    async function fetchData() {
      const response = await getEnterpriseById(enterpriseId);

      const { enterprise } = response.data;
      setEnterprise(enterprise);
    }
    fetchData();
  }, [enterpriseId]);

  return (
    <>
      {enterprise ? (
        <S.EnterpriseWrapper>
          <S.EnterpriseName>{enterprise.enterprise_name}</S.EnterpriseName>
          <S.EnterprisePhoto>
            {enterprise.photo ? (
              <img
                src={`https://empresas.ioasys.com.br${enterprise.photo}`}
                alt={enterprise.enterprise_name}
              />
            ) : (
              <S.EnterprisePhotoFallback>
                {getFallbackName(enterprise.enterprise_name)}
              </S.EnterprisePhotoFallback>
            )}
          </S.EnterprisePhoto>
          <S.EnterpriseDescription>
            {enterprise.description}
          </S.EnterpriseDescription>
          <Link to='/dashboard/enterprises' className='btn-link'>
            voltar para empresas
          </Link>
        </S.EnterpriseWrapper>
      ) : (
        <Preload />
      )}
    </>
  );
}

export default Enterprise;
