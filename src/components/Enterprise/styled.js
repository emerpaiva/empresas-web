import styled from 'styled-components';
import media from 'styled-media-query';

export const EnterpriseWrapper = styled.div`
  background-color: var(--white-two);
  padding: 4rem 5rem;
  margin: 0 auto;
  max-width: 120rem;
  width: 100%;

  ${media.lessThan('medium')`
    padding: 2rem 3rem;
  `}

  ${media.lessThan('medium')`
    padding: 2rem;
  `}
`;

export const EnterprisePhoto = styled.div`
  background-color: var(--green-one);
  border-radius: 0.2rem;
  height: 30rem;
  overflow: hidden;
  position: relative;
  margin-bottom: 2rem;
  width: 100%;

  & > img {
    height: fit-content;
    width: 100%;
  }
`;

export const EnterprisePhotoFallback = styled.span`
  color: var(--white-two);
  font-size: 5.6rem;
  font-weight: bold;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const EnterpriseName = styled.h1`
  font-size: 3.6rem;
  line-height: 1;
  text-align: center;
  margin-bottom: 3rem;
`;

export const EnterpriseDescription = styled.p`
  color: var(--warm-grey);
  line-height: 1.5;
  text-align: left;
  font-size: 2.2rem;
  margin-bottom: 4rem;
`;
