import styled from 'styled-components';
import media from 'styled-media-query';

export const EnterpriseOverviewWrapper = styled.div`
  border-radius: 0.5rem;
  background-color: var(--white-two);
  box-shadow: 0.3rem 0.5rem 1rem rgba(0, 0, 0, 0.08);
  display: flex;
  padding: 2.5rem;
  height: 20rem;
  max-width: 100%;
  width: 90rem;

  &:not(:last-child) {
    margin-bottom: 2rem;
  }

  ${media.lessThan('small')`
    flex-direction: column;
    height: auto;
  `}
`;

export const EnterpriseOverviewPhoto = styled.div`
  background-color: var(--green-one);
  border-radius: 0.2rem;
  position: relative;
  overflow: hidden;
  flex: 1 0 30%;

  ${media.lessThan('small')`
    height: 12rem;
    min-height: 12rem;
    flex: unset;
  `}
`;

export const EnterpriseOverviewPhotoFallBack = styled.span`
  color: var(--white-two);
  font-size: 5rem;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const EnterpriseOverviewInfo = styled.div`
  text-align: left;
  flex: 1 0 70%;
  padding: 2rem 3rem;
`;

export const EnterpriseOverviewName = styled.h3`
  color: var(--dark-indigo);
  cursor: pointer;
  font-size: 3rem;
  font-weight: bold;
`;

export const EnterpriseOverviewType = styled.h4`
  color: var(--warm-grey);
  cursor: pointer;
  font-size: 2.4rem;
  font-weight: normal;
  margin-bottom: 0.5rem;
`;

export const EnterpriseOverviewCountry = styled.h5`
  color: var(--warm-grey);
  font-size: 1.8rem;
  font-weight: normal;
`;
