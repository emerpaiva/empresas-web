import React from 'react';
// import { Link } from 'react-router-dom';

import { getFallbackName } from '../../utils/fallback';

import * as S from './styled';

function EnterpriseOverview({
  id,
  enterprise_name,
  country,
  photo,
  enterprise_type,
  handlerClickEnterprise,
  handlerClickFilterByType
}) {
  return (
    <S.EnterpriseOverviewWrapper>
      <S.EnterpriseOverviewPhoto>
        {photo ? (
          <img
            src={`https://empresas.ioasys.com.br${photo}`}
            alt={enterprise_name}
          />
        ) : (
          <S.EnterpriseOverviewPhotoFallBack>
            {getFallbackName(enterprise_name)}
          </S.EnterpriseOverviewPhotoFallBack>
        )}
      </S.EnterpriseOverviewPhoto>
      <S.EnterpriseOverviewInfo>
        <S.EnterpriseOverviewName onClick={() => handlerClickEnterprise(id)}>
          {enterprise_name}
        </S.EnterpriseOverviewName>
        <S.EnterpriseOverviewType
          onClick={() => handlerClickFilterByType(enterprise_type.id)}
        >
          {enterprise_type?.enterprise_type_name}
        </S.EnterpriseOverviewType>
        <S.EnterpriseOverviewCountry>{country}</S.EnterpriseOverviewCountry>
      </S.EnterpriseOverviewInfo>
    </S.EnterpriseOverviewWrapper>
  );
}

export default EnterpriseOverview;
