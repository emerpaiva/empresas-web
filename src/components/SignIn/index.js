import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';

import api from '../../services/api';
import { login as authLogin } from '../../services/auth';

import InputForm from '../InputForm';
import Button from '../Button';

import { ReactComponent as EmailIcon } from '../../assets/images/svg/ic-email.svg';
import { ReactComponent as PasswordIcon } from '../../assets/images/svg/ic-cadeado.svg';

import * as S from './styled';

function SignIn({ history }) {
  const [login, setLogin] = useState({
    email: '',
    password: '',
    error: '',
    preload: false
  });

  const handlerSubmit = async event => {
    event.preventDefault();

    const { email, password } = login;

    if (!email || !password) {
      setLogin({
        ...login,
        error: 'Preencha seu email e senha!'
      });
    } else {
      try {
        setLogin({
          ...login,
          preload: true
        });
        const response = await api.post('users/auth/sign_in', {
          email,
          password
        });

        authLogin(response.headers);
        history.push(`/dashboard`);
      } catch (error) {
        console.error('Error ao efetuar login:', error);
        setLogin({
          ...login,
          preload: false,
          error: 'Error ao efetuar login'
        });
      }
    }
  };

  const handlerChange = event => {
    const { value, name } = event.target;

    setLogin({
      ...login,
      [name]: value
    });
  };

  return (
    <S.Form onSubmit={handlerSubmit}>
      <InputForm
        type='email'
        placeholder='E-mail'
        name='email'
        icon={EmailIcon}
        handlerChange={handlerChange}
        value={login.email}
        required
      />
      <InputForm
        type='password'
        placeholder='Senha'
        name='password'
        icon={PasswordIcon}
        handlerChange={handlerChange}
        value={login.password}
        required
      />
      <Button type='submit' disabled={login.preload}>
        Entrar
      </Button>
      {login.error && <span className='error-msg'>{login.error}</span>}
    </S.Form>
  );
}

export default withRouter(SignIn);
