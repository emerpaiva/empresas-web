import styled from 'styled-components';

export const Form = styled.form`
  text-align: center;
  padding: 0 2rem;
  max-width: 100%;
  width: 34rem;
`;
