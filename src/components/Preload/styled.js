import styled from 'styled-components';

export const PreloadWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 100%;
  height: 20rem;
  z-index: 999;
  cursor: progress;
`;

export const PreloadCircle = styled.span`
  overflow: hidden;
  width: 5rem;
  height: 5rem;
  border-radius: 50%;
  border: 0.6rem solid #fff;
  animation: loader 0.7s infinite ease-in-out;
  border-top-color: #ee4c77;
  border-right-color: #ee4c77;
  border-bottom-color: #ee4c77;
  border-left-color: transparent;
`;
