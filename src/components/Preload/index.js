import React from 'react';

import * as S from './styled';

const Preload = () => (
  <S.PreloadWrapper>
    <S.PreloadCircle />
  </S.PreloadWrapper>
);

export default Preload;
