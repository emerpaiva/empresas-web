import styled from 'styled-components';

export const EnterpriseCollectionsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  & > .btn-link {
    display: block;
    max-width: 100%;
    width: 90rem;
    margin-bottom: 2rem;
  }
`;
