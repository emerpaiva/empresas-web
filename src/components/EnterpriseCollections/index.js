import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';

import { getEnterprises } from '../../services/api';

import EnterpriseOverview from '../EnterpriseOverview';
import Preload from '../Preload';

import * as S from './styled';

const useQuery = () => new URLSearchParams(useLocation().search);

function EnterpriseCollections({ history, match }) {
  const [enterprises, setEnterprises] = useState(null);
  let type = useQuery().get('type');
  let name = useQuery().get('name');

  useEffect(() => {
    setEnterprises(null);

    async function fetchData() {
      const response = await getEnterprises({ type, name });

      const { enterprises } = response.data;
      setEnterprises(enterprises);
    }

    fetchData();
  }, [type, name]);

  const handlerClickEnterprise = enterpriseId =>
    history.push(`${match.url}/${enterpriseId}`);

  const handlerClickFilterByType = enterpriseType =>
    history.push(`${match.url}?type=${enterpriseType}`);

  return (
    <S.EnterpriseCollectionsWrapper>
      {(type || name) && (
        <Link to='/dashboard/enterprises' className='btn-link'>
          Visualizar lista completa
        </Link>
      )}
      {enterprises ? (
        enterprises.map(enterprise => (
          <EnterpriseOverview
            key={enterprise.id}
            {...enterprise}
            handlerClickEnterprise={handlerClickEnterprise}
            handlerClickFilterByType={handlerClickFilterByType}
          />
        ))
      ) : (
        <Preload />
      )}
    </S.EnterpriseCollectionsWrapper>
  );
}

export default EnterpriseCollections;
