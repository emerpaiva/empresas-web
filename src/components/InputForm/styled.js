import styled, { css } from 'styled-components';

export const InputContainer = styled.div`
  border-bottom: 1px solid #383743;
  display: flex;
  position: relative;

  &:not(:last-child) {
    margin-bottom: 3rem;
  }

  ${props =>
    props.large &&
    css`
      border-bottom-color: transparent;
    `}

  ${props =>
    props.open &&
    css`
      flex: 1 0 100%;
      border-bottom-color: var(--white);
    `}
`;

export const InputField = styled.input`
  appearance: none;
  border: none;
  background-color: transparent;
  font-size: 1.6rem;
  width: 100%;

  &::placeholder {
    color: rgba(#383743, 0.5);
  }

  ${props =>
    props.large &&
    css`
      font-size: 3.2rem;

      &::placeholder {
        color: #991237;
      }

      ${props =>
        !props.open &&
        css`
          display: none;
        `}
    `}

  &:not(:placeholder-shown) ~ label {
    top: -2rem;
    color: var(--night-blue);
  }
`;

export const InputLabel = styled.label`
  color: transparent;
  font-size: 1.8rem;
  position: absolute;
  transition: all 0.2s linear;
  left: 4rem;
  top: 1rem;

  ${props =>
    props.large &&
    css`
      color: transparent !important;
    `}
`;

export const InputIconWrapper = styled.div`
  height: 100%;
  margin-right: 1rem;

  & > svg {
    width: 3rem;
  }

  ${props =>
    props.large &&
    css`
      height: 5rem;
      margin-right: 1rem;
      width: 5rem;
      display: flex;
      align-items: center;

      & > svg {
        width: 100%;
      }
    `}
`;
