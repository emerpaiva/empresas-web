import React, { useState } from 'react';

import * as S from './styled';

function InputForm({
  type = 'text',
  placeholder,
  handlerChange,
  name,
  large,
  icon: Icon,
  ...otherProps
}) {
  const [isOpen, setOpen] = useState(false);

  const toggleOpen = () => setOpen(!isOpen);

  return (
    <S.InputContainer large={large} open={isOpen}>
      {Icon && (
        <S.InputIconWrapper onClick={toggleOpen} large={large}>
          <Icon />
        </S.InputIconWrapper>
      )}

      <S.InputField
        type={type}
        placeholder={placeholder}
        onChange={handlerChange}
        name={name}
        id={name}
        large={large}
        open={isOpen}
        {...otherProps}
      />
      <S.InputLabel large={large} htmlFor={name}>
        {placeholder}
      </S.InputLabel>
    </S.InputContainer>
  );
}

export default InputForm;
