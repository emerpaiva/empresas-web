import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

import { isAuthenticated } from './services/auth';

import DashboardPage from './pages/Dashboard';
import LoginPage from './pages/Login';

function App() {
  return (
    <Router>
      <Switch>
        <Route
          path='/login'
          render={() =>
            isAuthenticated() ? <Redirect to='/dashboard' /> : <LoginPage />
          }
        />
        <Route
          path='/dashboard'
          render={() =>
            isAuthenticated() ? <DashboardPage /> : <Redirect to='/login' />
          }
        />
        <Route
          path='/'
          render={() =>
            isAuthenticated() ? (
              <Redirect to='/dashboard' />
            ) : (
              <Redirect to='/login' />
            )
          }
        />
      </Switch>
    </Router>
  );
}

export default App;
