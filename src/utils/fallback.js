export const getFallbackName = name => {
  const arrName = name.split(' ');

  return `${arrName[0][0]}${arrName[arrName.length - 1][0]}`.toUpperCase();
};
