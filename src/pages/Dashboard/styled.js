import styled from 'styled-components';

export const DashboardPage = styled.div`
  padding: 12rem 2rem 10rem;
  position: relative;
  height: 100%;
  min-height: 100vh;
`;
