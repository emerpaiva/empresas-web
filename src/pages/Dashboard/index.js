import React from 'react';
import { Switch, Route, Link, withRouter } from 'react-router-dom';

import Header from '../../components/Header';
import Enterprise from '../../components/Enterprise';
import EnterpriseCollections from '../../components/EnterpriseCollections';

import * as S from './styled';

function DashboardPage({ history, match }) {
  const handlerSearch = event => {
    if (event.which === 13 || event.keyCode === 13) {
      const { value } = event.target;

      history.push(`${match.url}/enterprises?name=${value}`);
    }
  };

  return (
    <S.DashboardPage>
      <Header handlerSearch={handlerSearch} />
      <Switch>
        <Route exact path={`${match.path}`}>
          <h3 className='alert'>
            <Link to={`${match.path}/enterprises`} className='text-primary'>
              Visualize
            </Link>{' '}
            as empresas cadastradas ou clique em buscar para iniciar.
          </h3>
        </Route>
        <Route
          exact
          path={`${match.path}/enterprises`}
          component={EnterpriseCollections}
        />
        <Route
          path={`${match.path}/enterprises/:enterpriseId`}
          component={Enterprise}
        />
      </Switch>
    </S.DashboardPage>
  );
}

export default withRouter(DashboardPage);
