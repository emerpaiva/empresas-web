import styled from 'styled-components';

export const LoginPage = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
  height: 100%;
  width: 100%;
`;

export const LogoWrapper = styled.div`
  margin-bottom: 6.5rem;
`;

export const LoginTitle = styled.h1`
  font-size: 2.4rem;
  text-align: center;
  margin-bottom: 2rem;
  width: 18rem;
`;

export const LoginSubitle = styled.h2`
  font-size: 1.8rem;
  font-weight: normal;
  text-align: center;
  margin-bottom: 5rem;
  max-width: 100%;
  width: 32rem;
`;
