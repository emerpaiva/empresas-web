import React from 'react';

import SignIn from '../../components/SignIn';

import Logo from '../../assets/images/png/logo-home.png';

import * as S from './styled';

function LoginPage() {
  return (
    <S.LoginPage>
      <S.LogoWrapper>
        <img src={Logo} alt='ioasys' />
      </S.LogoWrapper>
      <S.LoginTitle>BEM-VINDO AO EMPRESAS</S.LoginTitle>
      <S.LoginSubitle>
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
      </S.LoginSubitle>
      <SignIn />
    </S.LoginPage>
  );
}

export default LoginPage;
